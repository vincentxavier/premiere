\documentclass[../cours_premiere_generale_maths.tex]{subfiles}

\title{Dérivation}
\author{1ère S}
\date{}

\begin{document}


\ifSubfilesClassLoaded{%
  \maketitle%
}{}

Soit $f$ une fonction définie sur un intervalle $I$ de $\R$ et $a \in I$. On note $\mathcal{C}_f$ sa courbe représentative.

\section{Nombre dérivé et tangente}
\begin{definition}
Le  taux d'accroissement de $f$ entre $a$ et $a+h$ est la quantité :
\[\dfrac{f(a+h)-f(a)}{h}\]
\end{definition}

\begin{remarque}
Ce nombre  est le coefficient directeur de la droite sécante à la courbe passant par les points $(a\ ;\  f(a))$ et $(a+h\ ;\  f(a+h))$.
\end{remarque}

On peut illustrer ceci graphiquement.

\begin{center}
  \begin{tikzpicture}[yscale=0.7]
\tkzInit[xmin=-1,xmax=4,ymin=-1,ymax=25,ystep=2]
\tkzAxeXY
\draw[gray, dotted] (-1,0) grid (4,12);
\draw[blue, very thick] (1,3.5) node {$\bullet$} node[left]{$A$} --(2,11)node {$\bullet$} node[left]{$M$};
\draw[red, very thick,domain=0:2.3] plot(\x,{2.5*\x*\x+1});
\draw (1,3.5) -|(2,11);
\draw[dotted, gray] (1,3.5) --(1,0);
\draw[dotted, gray] (2,11) --(2,0);
\draw[dotted, gray] (0,3.5) --(1,3.5);
\draw[dotted, gray] (2,11) --(0,11);
\end{tikzpicture}
\end{center}

\begin{definition}

Si, quand $h$ \emph{tend} vers 0, la quantité $\dfrac{f(a+h)-f(a)}{h}$
\emph{tend} vers un nombre, on dit que la fonction $f$ est \emph{dérivable
en $a$} et ce nombre est le \emph{nombre dérivé de $f$ en $a$}.
On le note $f'(a)$.
\end{definition}


\begin{exemple}
La fonction $f$ définie sur $\R$ par $f(x)=5x^2-16$ est-elle dérivable en $a=2$ ?

Pour le savoir étudions la quantité suivante : $\dfrac{f(2+h)-f(2)}{h}$.\\
$\dfrac{f(2+h)-f(2)}{h}=\dfrac{5(2+h)^2-16-(5\times 2^2-16)}{h}$\\
$\dfrac{f(2+h)-f(2)}{h}=\dfrac{20+10h+5h^2-16-20+16}{h}$\\
$\dfrac{f(2+h)-f(2)}{h}=\dfrac{10h+5h^2}{h}$\\
$\dfrac{f(2+h)-f(2)}{h}=10+5h$\hfill{$h$ est différent de 0}\\
Lorsque $h$ \emph{tend} vers 0 (en restant différent de 0), $10+5h$ \emph{tend} vers 10.\\
Donc $f$ est dérivable en $a=2$ et son nombre dérivé en 2 est 10.\\
On note alors $f'(2)=10$.
\end{exemple}

\subsection{Interprétation graphique du nombre dérivé}

 Soit $\mathcal{C}$ la courbe représentative de la fonction $f$ et $A(a;f(a))$ et $B(a+h;f(a+h))$, deux points de cette courbe.
La quantité $\frac{f(a+h)-f(a)}{h}$ est le coefficient directeur de la droite $(AB)$, sécante à la courbe.
Lorsque $h$ \emph{tend} vers 0, la sécante $(AB)$ \emph{tend} vers la tangente à la courbe au point $A$ et le nombre $\frac{f(a+h)-f(a)}{h}$ \emph{tend} vers le coefficient directeur de la tangente en $A$.

On a donc la propriété suivante, qu'on admettra :
\begin{proposition}
Soit $f$ une fonction dérivable en $a$ et $\mathcal{C}$ la courbe
représentative de $f$. \\ Le nombre dérivé $f'(a)$ est le coefficient
directeur de la tangente à la courbe $\mathcal{C}$ au point d'abscisse $a$.
\end{proposition}

\begin{center}
  \begin{tikzpicture}[yscale=0.7]
\tkzInit[xmin=-1,xmax=4,ymin=-1,ymax=25,ystep=2]
\tkzAxeXY
\draw[gray, dotted] (-1,0) grid (4,12);
\draw[blue, very thick] (1,3.5) node {$\bullet$} node[left]{$A$};
\draw[red, very thick,domain=0:2.3] plot(\x,{2.5*\x*\x+1});
\draw[green, dashed,domain=0:3] plot(\x,{5*\x-1.5});
\end{tikzpicture}
\end{center}

\begin{proposition}
Soit $f$ une fonction définie et dérivable en $a$ et $\mathcal{C}$ sa courbe représentative.\\
Alors $\mathcal{C}$ admet en son point d'abscisse $a$ une tangente $T_a$ d'équation
\[y=f'(a)(x-a)+f(a)\]
\end{proposition}
\begin{proof}
On considère la tangente à $\mathcal{C}$ au point $A$ d'abscisse $a$.\\
Le coefficient directeur de $T_a$ est $f'(a)$ donc $T_a : y=f'(a)x+p$.\\
Le point $A$ appartient à la droite donc $y_a=f'(a)\times a +p$\\
Le point $A$ est un point de la courbe $\mathcal{C}$ donc $y_a=f(a)$\\
On obtient donc $f(a)=f'(a)\times a+p \Leftrightarrow p=f(a)-f'(a)\times a$\\
On remplace $y=f'(a)x+f(a)-f'(a)a$\\
Ce qui donne $y=f'(a)(x-a)+f(a)$
\end{proof}

\section{Fonction dérivée}

\subsection{Définition}

\begin{definition}
  Soit $f$ une fonction définie sur $I$, un intervalle de \R. On dit que $f$
  est dérivable sur $I$ si $f$ admet un nombre dérivé en tout point de $I$,
  sauf éventuellement aux bornes.
\end{definition}

\begin{proposition}[admise]
  Toute fonction dérivable sur $I$ est continue sur $I$, c'est-à-dire qu'on
  peut la tracer sans lever le crayon.
\end{proposition}

\begin{proposition}
  Soit $I$ un intervalle de \R. Si $f$ et $g$ sont deux fonctions dérivables
  sur \R, et $\lambda$ un réel, alors
  \begin{enumerate}
    \item $f+g$ est dérivable sur $I$ ;
    \item $f - g$ est dérivable sur $I$ ;
    \item $\lambda f$ est dérivable sur $I$
    \item $f×g$ est dérivable sur $I$
  \end{enumerate}
\end{proposition}

\begin{savoirrediger}
  Avant de calculer une dérivée, il faut s'assurer que la dérivée existe en
  utilisant la proposition précédente.
\end{savoirrediger}

\begin{proposition}
  Soit $I$ un intervalle de \R. Si $f$ et $g$ sont deux fonctions dérivables
  sur \R, et $\lambda$ un réel, alors
  \begin{enumerate}
    \item $(f+g)' = f' + g'$ ;
    \item $(f - g)' = f' - g'$ ;
    \item $(\lambda f)' = \lambda f'$
  \end{enumerate}
\end{proposition}

\begin{proposition}
  Soit $I$ un intervalle de \R. Si $f$ et $g$ sont deux fonctions dérivables
  sur \R, alors
  \[
    (f × g)' = f' × g + f × g'
  .\]
\end{proposition}

\begin{proposition}
  Soit $I$ un intervalle de \R. Si $f$ et $g$ sont deux fonctions dérivables
  sur \R, telle que $\forall x \in I, g(x) ≠ 0$, alors
  \begin{itemize}
    \item $\frac1g$ est dérivable et $\left(\frac1g\right)' =
        \frac{-g'}{g^2}$
      \item $\frac{f}g$ est dérivable et $\left(\frac{f}g\right)' =
        \frac{f'g-fg'}{g^2}$
  \end{itemize}
\end{proposition}

\subsection{Dérivée des fonctions de référence}

La connaissance des dérivées des fonctions de référence permet de calculer
les dérivées des fonctions qui sont combinaisons de celles-ci :

\begin{itemize}
  \item $\R\rightarrow\R,~x\mapsto mx+p$ est dérivable sur $\R$, de dérivée
    $\R\rightarrow\R,~x\mapsto m$.
  \item $\R\rightarrow\R,~x\mapsto x^2$ est dérivable sur $\R$, de dérivée
    $\R\rightarrow\R,~x\mapsto 2x$.
  \item $\R\rightarrow\R,~x\mapsto x^n$ est dérivable sur $\R$, de dérivée
    $\R \rightarrow \R,~x \mapsto nx^{n-1}$. ($n \in \N$).
  \item $\R-\{0\}\rightarrow\R,x\mapsto \dfrac {1}{x}$ est dérivable sur
    $\R-\{0\}$, de dérivée $\R\rightarrow\R,~x\mapsto -\dfrac{1}{x^2}$.
  \item $[0;+\infty[\rightarrow\R,x\mapsto \sqrt x$ est dérivable sur
    $]0;+\infty[$ de dérivée $]0;+\infty[, x\mapsto \dfrac {1}{2\sqrt x}$
\end{itemize}
\begin{proof}
  \begin{itemize}
    \item Soit la fonction $f(x)=mx+p$.\\
      Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
      $=\dfrac{m(a+h)+p-ma-p}{h}$\\
      $=\dfrac{mh}{h}$\\
      $=\dfrac{m(a+h)+p-ma-p}{h}$\\
      $=m$ $h$ est différent de zéro\\
      $f'(x)=m$

    \item Soit la fonction $f(x)=x^2$:\\
      Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
      $=\dfrac{(a+h)^2-a^2	}{h}$\\
      $=\dfrac{a^2+2ah+h^2-a^2}{h}$\\
      $=\dfrac{2ah+h^2}{h}$\\
      $2a+h$\quad $h$ est différent de zéro\\
      Lorsque $h$ tend vers zéro alors $\dfrac{f(a+h)-f(a)}{h}$ tend vers $2a+h$\\
      $f'(x)=2x$

    \item Soit la fonction $f(x)=\dfrac{1}{x}$:\\
      Calculer $f(a+h)-f(a)$:\\
      $=\dfrac{1}{a+h}-\dfrac{1}{a}$\\
      $=\dfrac{a-a-h}{a(a+h)}$\\
      $=\dfrac{-h}{a(a+h)}$\\
      Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
      $=\dfrac{\dfrac{-h}{a(a+h)}}{h}$\\
      $=\dfrac{-1}{a^2+ah}$\quad $h$ est différent de zéro\\
      Lorsque $h$ tend vers zéro alors $\dfrac{f(a+h)-f(a)}{h}$ tend vers $-\dfrac{1}{a^2}$\\
      $f'(x)=-\dfrac{1}{x^2}$

    \item Soit la fonction $f(x)=\sqrt{x}$:\\
      Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
      $=\dfrac{\sqrt{a+h}-\sqrt{a}}{h}$\\
      $=\dfrac{a+h-a}{h(\sqrt{a+h}+\sqrt{a})}$\\
      $=\dfrac{h}{h(\sqrt{a+h}+\sqrt{a})}$\\
      $=\dfrac{1}{\sqrt{a+h}+\sqrt{a}}$\quad $h$ est différent de zéro\\
      Lorsque $h$ tend vers zéro alors $\dfrac{f(a+h)-f(a)}{h}$ tend vers $\dfrac{1}{2\sqrt{a}}$\\
      $f'(x)=\dfrac{1}{2\sqrt{x}}$
  \end{itemize}

\end{proof}

\subsection{Exemples}

On trouve aussi souvent la notation suivante où $u$ et $v$ sont des
fonctions définies et dérivables sur un intervalle $I$. Alors
\begin{itemize}
  \item $u+v$ est dérivable sur $I$ de dérivée $(u+v)'=u'+v'$.
  \item $uv$ est dérivable sur $I$ de dérivée $(uv)'=u'v+v'u$. 
  \item $ku$ est dérivable sur $I$ de dérivée $(ku)'=ku'$. (ou $k\in\mathbb R$)
  \item $\dfrac{1}{v}$ est dérivable sur $I$ privé des $x$ tels que
    $v(x)=0$, de dérivée $-\dfrac {v'}{v^2}$.
  \item $\dfrac{u}{v}$ est dérivable sur $I$ privé des $x$ tels que
    $v(x)=0$, de dérivée $\dfrac {u'v-v'u}{v^2}$.
  \item $x\mapsto u(ax+b)$ est dérivable sur l'intervalle des $x$ tels que
    $ax+b\in I$ de dérivée $x\mapsto a\times u'(ax+b)$.
\end{itemize}
\begin{exemple}

  \begin{itemize}
    \item Soit la fonction $g(x)=5x^3+2,5x^2+7x+11$\\
      Calculer la dérivée :\\
      $g'(x)=3\times 5x^2+2\times 2,5x +7$\\
      $g'(x)=15x^2+5x+7$
    \item Soit la fonction $h(x)=\dfrac{1}{5x^2+3x+4}$\\
      Calculer la dérivée de $5x^2+3x+4$: $10x+3$\\
      $h'(x)=-\dfrac{10x+3}{(5x^2+3x+4)^2}$\\
    \item Soit la fonction $t(x)=\dfrac{3x+2}{5x+7}$\\
      Calculer la dérivée de $3x+2$ : $3$\\
      Calculer la dérivée de $5x+7$ : $5$\\
      Calculer le dérivée de $t$ :\\
      $t'(x)=\dfrac{3\times (5x+7) - 5\times (3x+2)}{(5x+7)^2}$\\
      $t'(x)=\dfrac{15x+21-15x-10}{(5x+7)^2}$\\
      $t'(x)=\dfrac{11}{(5x+7)^2}$

    \item Soit la fonction $f(x)=\sqrt{x}(6x+3,4)$\\
      Calculer la dérivée de $\sqrt{x}$ : $-\dfrac{1}{2\sqrt{x}}$\\
      Calculer la dérivée de $6x+3,4$ : $6$\\
      Calculer la dérivée de $f$ :\\
      $f'(x)=-\dfrac{1}{2\sqrt{x}}\times (6x+3,4)+6\sqrt{x}$
  \end{itemize}
\end{exemple}

\section{Applications}

\subsection{Sens de variation}

\begin{proposition}
   Soit $f$ une fonction dérivable sur un intervalle $[a,b]$.
   \begin{itemize}
     \item Si $f$ est strictement croissante sur $\interoo{a b}$ alors $f'(x)>0$ sur $]a,b[$.
     \item Si $f$ est strictement décroissante sur $\interoo{a b}$ alors
       $f'(x)<0$ sur $]a,b[$.
     \item Si $f$ est constante sur $\interoo{a b}$ alors $f'(x)=0$ sur
       $\interff{a b}$.
   \end{itemize}
\end{proposition}

\begin{theoreme}[admis]
  Soit $f$ une fonction dérivable sur un intervalle $[a,b]$.\\
  \begin{itemize}
    \item Si $f'(x)>0$ sur $]a,b[$ alors $f$ est strictement croissante sur $[a,b]$.
    \item Si $f'(x)<0$ sur $]a,b[$ alors $f$ est strictement décroissante sur $[a,b]$.
    \item Si $f'(x)=0$ sur $]a,b[$ alors $f$ est constante sur $[a,b]$.
  \end{itemize}
\end{theoreme}
\begin{exemple}
  Soit la fonction $f(x)=\dfrac{1-x}{2-x}$
  \begin{enumerate}
    \item Ensemble de définition.\\
      \ligne[2]
    \item Calculer la fonction dérivée $f'$.\\
      \ligne[2]
    \item \'Etudier le signe de la dérivée.\\
      \ligne[3]
    \item Déterminer le sens de variation de la fonction $f$.\\
      \ligne[5]
  \end{enumerate}
\end{exemple}

\subsection{Extremum locaux}

\begin{theoreme}
  Soit $f$ une fonction dérivable sur un intervalle $I = \interff{a b}$.
  Si, pour $x_e \in \interoo{a b}$, on a
  \begin{itemize}
    \item $f'(x_e) = 0$
    \item pour $x \in \interoo{a x_e}$ et $y \in \interoo{x_e b}$,
      $f'(x)×f'(y) < 0$
  \end{itemize}
  alors, $f$ présente un extremum (maximum ou minimum) local.
\end{theoreme}

\end{document}



