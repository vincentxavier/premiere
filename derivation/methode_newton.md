---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.0
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Méthode de Newton pour trouver les racines d'une fonction

On considère que $f$ est une fonction dérivable sur $I = [a;b]$ un
intervalle de $\mathbf{R}$. On suppose de plus que $f'$ ne s'annule pas sur
$I$.

Pr!q
