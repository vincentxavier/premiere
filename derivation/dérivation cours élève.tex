\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath,amssymb}
\usepackage{amsfonts}

\usepackage{kpfonts}
\usepackage{eurosym}
\usepackage{fancybox}
\usepackage{fancyhdr}
\usepackage{enumerate}
\usepackage{icomma}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}


\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{Lycée \textsc{Jean-Baptiste de la Salle}}}}
\rfoot{\footnotesize{Page \thepage/ \pageref{LastPage}}}
\rhead{}
\lhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter


\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[yscale=0.8]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother

\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\newtheorem{demonstration}{\textsf{\textsc{\small{Démonstration}}}}
\newtheorem{exemple}{\textsf{\textsc{\small{Exemple}}}}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}
\newtheorem{proof}{Preuve}
\newtheorem{theoreme}{Théorème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\FrenchLabelItem}{\textbullet}

%Titres
\renewcommand{\thesection}{\Roman{section}.}
\renewcommand{\thesubsection}{%
\thesection~\alph{subsection}}
\sectionfont{\color{blue}{}}
\subsectionfont{\color{violet}{}}

\newcommand{\rep}[1]{\textcolor{blue}{#1}}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0.5pt}

\everymath{\displaystyle\everymath{}}
\title{Dérivation}
\author{1\up{ère}S}
\date{}

\begin{document}

\maketitle\\

Soit $f$ une fonction définie sur un intervalle $I$ de $\R$ et $a \in I$. \\
On note $\mathcal{C}_f$ sa courbe représentative.
\section{Nombre dérivé et tangente}
\begin{definition}
Le  \textcolor{red}{taux d'accroissement}{} de $f$ entre $a$ et $a+h$ est la quantité :

\[\dfrac{f(a+h)-f(a)}{h}\]
\end{definition}

\begin{remarque}
Ce nombre  est le coefficient directeur de la droite sécante à la courbe passant par les points $(a\ ;\  f(a))$ et $(a+h\ ;\  f(a+h))$.
\end{remarque}
\begin{center}
\begin{tikzpicture}[scale=0.8]
\tkzInit[xmin=-1,xmax=4,ymin=-1,ymax=25,ystep=2]
\tkzAxeXY
\draw[gray, dotted] (-1,0) grid (4,12);
\draw[blue, very thick] (1,3.5) node {$\bullet$} node[left]{$A$} --(2,11)node {$\bullet$} node[left]{$M$};
\draw[red, very thick,domain=0:2.1] plot(\x,{2.5*\x*\x+1});
\draw (1,3.5) -|(2,11);
\draw[dotted, gray] (1,3.5) --(1,0);
\draw[dotted, gray] (2,11) --(2,0);
\draw[dotted, gray] (0,3.5) --(1,3.5);
\draw[dotted, gray] (2,11) --(0,11);
\end{tikzpicture}
\end{center}

\begin{definition}

Si, quand $h$ \emph{tend} vers 0, la quantité $\dfrac{f(a+h)-f(a)}{h}$\\

\emph{tend} vers un nombre, on dit que la fonction $f$ est \textcolor{red}{dérivable en $a$} et ce nombre est le \textcolor{red}{nombre dérivé de $f$ en $a$}. \\
On le note $f'(a)$.
\end{definition}


\begin{exemple}
La fonction $f$ définie sur $\R$ par $f(x)=5x^2-16$ est-elle dérivable en $a=2$ ?\\
\ligne[10]
\end{exemple}

\subsection*{Interprétation graphique du nombre dérivé}

 Soit $\mathcal{C}$ la courbe représentative de la fonction $f$ et $A(a;f(a))$ et $B(a+h;f(a+h))$, deux points de cette courbe.\\
La quantité $\frac{f(a+h)-f(a)}{h}$ est le \textcolor{red}{coefficient directeur} de la droite $(AB)$, sécante à la courbe.\\

Lorsque $h$ \emph{tend} vers 0, la sécante $(AB)$ \emph{tend} vers la \textcolor{red}{tangente} à la courbe au point $A$ et le nombre $\frac{f(a+h)-f(a)}{h}$ \emph{tend} vers le coefficient directeur de la tangente en $A$.


\begin{propriete}
Soit $f$ une fonction dérivable en $a$ et $\mathcal{C}$ la courbe représentative de $f$. \\ 
\textcolor{red}{Le nombre dérivé} $f'(a)$ est le coefficient directeur de la tangente à la courbe $\mathcal{C}$ au point d'abscisse a.
\end{propriete}

\begin{center}
\begin{tikzpicture}[scale=0.7]
\tkzInit[xmin=-1,xmax=4,ymin=-1,ymax=25,ystep=4]
\tkzAxeXY
\draw[gray, dotted] (-1,0) grid (4,7);
\draw[blue, very thick] (1,3.5) node {$\bullet$} node[left]{$A$};
\draw[red, very thick,domain=0:1.5] plot(\x,{2.5*\x*\x+1});
\draw[green, dashed,domain=0:1.5] plot(\x,{5*\x-1.5});
\end{tikzpicture}
\end{center}

\begin{propriete}
Soit $f$ une fonction définie et dérivable en $a$ et $\mathcal{C}$ sa courbe représentative.\\
Alors $\mathcal{C}$ admet en son point d'abscisse $a$ une tangente $T_a$ d'équation
\textcolor{red}{\[y=f'(a)(x-a)+f(a)\]}
\end{propriete}
\begin{proof}
On considère la tangente à $\mathcal{C}$ au point $A$ d'abscisse $a$.\\
\begin{enumerate}
\item Le coefficient directeur de $T_a$ est $f'(a)$ donner l'équation réduite de cette droite : \\
\ligne[2]
\item Le point $A$ appartient à la droite : \\
\ligne[2]
\item Le point $A$ est un point de la courbe $\mathcal{C}$\\
\ligne[2]
\item Conclure\\
\ligne[2]
\end{enumerate}
\end{proof}

\section{Dérivée des fonctions de référence}
La \textcolor{red}{fonction dérivée} $f'$ d'une fonction $f$ définie sur un intervalle $I$ et dérivable en tout point de $I$ est la fonction qui a tout $x\in I$ associe le nombre dérivée $f'(x)$. \\

\begin{itemize}
\item $\R\rightarrow\R,~x\mapsto mx+p$ est dérivable sur $\R$, de dérivée $\R\rightarrow\R,~x\mapsto m$.

\item $\R\rightarrow\R,~x\mapsto x^2$ est dérivable sur $\R$, de dérivée $\R\rightarrow\R,~x\mapsto 2x$.

\item $\R\rightarrow\R,~x\mapsto x^n$ est dérivable sur $\R$, de dérivée
 $\R \rightarrow \R,~x \mapsto nx^{n-1}$ ($n \in \N$).
 
\item $\R-\{0\}\rightarrow\R,x\mapsto \dfrac {1}{x}$ est dérivable sur $\R-\{0\}$, de dérivée $\R\rightarrow\R,~x\mapsto -\dfrac{1}{x^2}$.

\item $[0;+\infty[\rightarrow\R,x\mapsto \sqrt x$ est dérivable sur $]0;+\infty[$ de dérivée $]0;+\infty[, x\mapsto \dfrac {1}{2\sqrt x}$

\end{itemize}
\begin{proof}
\begin{itemize}
\item Soit la fonction $f(x)=mx+p$.\\
Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
\ligne[6]

Conclure : \dotfill


\item Soit la fonction $f(x)=x^2$:\\
\begin{enumerate}
\item Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
\ligne[6]
\item Que se passe t il lorsque $h$ tend vers zéro :\\
\ligne[2]
\item Conclure \dotfill
\end{enumerate}


\item Soit la fonction $f(x)=\dfrac{1}{x}$:\\
\begin{enumerate}
\item Calculer $f(a+h)-f(a)$:\\
\ligne[6]
\item Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
\ligne[6]
\item Que se passe t il lorsque $h$ tend vers zéro :\\
\ligne[2]
\item Conclure \dotfill
\end{enumerate}


\item Soit la fonction $f(x)=\sqrt{x}$:\\
\item Calculer $\dfrac{f(a+h)-f(a)}{h}$:\\
\ligne[6]
\item Que se passe t il lorsque $h$ tend vers zéro :\\
\ligne[2]
\item Conclure \dotfill
\end{itemize}

\end{proof}
\section{Opérations sur les dérivées}
Soient $u$ et $v$ deux fonctions définies et dérivables sur un intervalle $I$. Alors\\
\begin{itemize}
\item $u+v$ est dérivable sur $I$ de dérivée $(u+v)'=u'+v'$.\\
\item $uv$ est dérivable sur $I$ de dérivée $(uv)'=u'v+v'u$. \\
\item $ku$ est dérivable sur $I$ de dérivée $(ku)'=ku'$ (ou $k\in\mathbb R$)\\
\item $\dfrac{1}{v}$ est dérivable sur $I$ privé des $x$ tels que $v(x)=0$, de dérivée $-\dfrac {v'}{v^2}$.\\
\item $\dfrac{u}{v}$ est dérivable sur $I$ privé des $x$ tels que $v(x)=0$, de dérivée $\dfrac {u'v-v'u}{v^2}$.\\
\item $x\mapsto u(ax+b)$ est dérivable sur l'intervalle des $x$ tels que $ax+b\in I$ \\
de dérivée $x\mapsto a\times u'(ax+b)$.\\
\end{itemize}
\begin{exemple}

\begin{enumerate}
\item Soit la fonction $g(x)=5x^3+2,5x^2+7x+11$\\
Calculer la dérivée :\\
\ligne[4]
\pagebreak
\item Soit la fonction $h(x)=\dfrac{1}{5x^2+3x+4}$\\

Calculer la dérivée de $5x^2+3x+4$: $10x+3$\\
\ligne[4]

\item Soit la fonction $t(x)=\dfrac{3x+2}{5x+7}$\\

Calculer la dérivée de $3x+2$ : \dotfill \\

Calculer la dérivée de $5x+7$ : \dotfill\\


Calculer le dérivée de $t$ :\\
\ligne[4]

\item Soit la fonction $f(x)=\sqrt{x}(6x+3,4)$\\

Calculer la dérivée de $\sqrt{x}$ : \dotfill\\

Calculer la dérivée de $6x+3,4$ :\dotfill\\

Calculer la dérivée de $f$ :\\
\ligne[3]
\end{enumerate}

\end{exemple}
\pagebreak
\subsection{Sens de variation}
\begin{theoreme}
Soit $f$ une fonction dérivable sur un intervalle $[a,b]$.\\
$\bullet$ Si $f'(x)>0$ sur $]a,b[$ alors $f$ est strictement croissante sur $[a,b]$.\\
$\bullet$ Si $f'(x)<0$ sur $]a,b[$ alors $f$ est strictement décroissante sur $[a,b]$.\\
$\bullet$ Si $f'(x)=0$ sur $]a,b[$ alors $f$ est constante sur $[a,b]$.
\end{theoreme}
\begin{exemple}
Soit la fonction $f(x)=\dfrac{1-x}{2-x}$
\begin{enumerate}
\item Ensemble de définition.\\
\ligne[2]
\item Calculer la fonction dérivée $f'$.\\
\ligne[4]
\item \'Etudier le signe de la dérivée.\\
\ligne[5]
\item Déterminer les variations de la fonction $f$.\\
\ligne[8]
\end{enumerate}
\end{exemple}

\ligne[32]

\end{document}



