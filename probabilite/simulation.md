---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
    format_version: 0.13
    jupytext_version: 1.13.0
kernelspec:
  display_name: Python 3 (ipykernel)
  language: python
  name: python3
---

# Simulation en probabilités

## Le lancer d'une pièce

```{code-cell} ipython3
from random import random
random()
```

Indiquer ce que fait la fonction `random()`.

```{code-cell} ipython3
def piece():
  if random() > 0.5:
    return 'Pile'
  else:
    return 'Face'
```

Que fait la fonction `piece()` ?

On désire lancer plusieurs fois un dé et stocker le résultat dans une liste.

````{margin}
```{hint}
`L[0:10]` permet d'afficher les 10 premiers éléments de la liste `L`.
```
````

```{code-cell} ipython3
L = []
for i in range(10000):
  L = L + [piece()]
L[0:10]
```
````{margin}
```{hint}
`L.count(objet)` permet de compter les objets de la liste `L`.
```
````
```{code-cell} ipython3
L.count('Pile')
```

Le nombre de `'Pile'` réalisé par cette expérience aléatoire est …

```{code-cell} ipython3
L.count('Pile')/10000
```
Expliquer pourquoi on ne retrouve pas précisément la probabilté de
$\frac12$.

## Le lancer de dé

On désire désormais simuler un lancer de dé.

```{code-cell} ipython3
from random import randint
randint(1,6)
```

Indiquer ce que fait la fonction `randint(1,6)`.

En vous inspirant de la partie précédente, simuler 10000 lancer d'un dé à 6
faces et donner la probabilité d'apparition du 6.

## Répéter une même expérience aléatoire et faire la moyenne

Reprenons le lancer de pièce et simulons 1000 fois l'expérience de lancer
1000 fois la pièce.

Pour cela, on commence par écrire une fonction permettant de lancer `N` fois
la pièce et qui renvoie le nombre de `'Pile'`

```{code-cell} ipython3
def lancer_piece(N):
  L = []
  for i in range(N):
    L = L + [piece()]
  return L.count('Pile')/N
```

On peut maintenant simuler 1000 fois cette expérience et faire la moyenne.

```{code-cell} ipython3
somme = 0
for i in range(10000):
  somme = somme + lancer_piece(1000)
somme/10000
```

Expliquer pourquoi ce résultat est une moyenne ?

Expliquer maintenant le résultat obtenu ici.
