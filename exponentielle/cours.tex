\documentclass[../cours_premiere_generale_maths.tex]{subfiles}

\title{Second degré}
\author{1ère S}
\date{}

\begin{document}


\ifSubfilesClassLoaded{%
  \maketitle%
}{}



\section{Généralités}

\subsection{Définition}

\begin{theoreme}
	Soit $f$ une fonction définie, continue, dérivable sur $\R$ vérifiant
	$f' = f$ et $f(0) = 1$.
	
	Une telle fonction est unique.
\end{theoreme}

On a montré, en admettant son existence, qu'il existait une et une seule
fonction définie, continue et dérivable sur $\R$ telle que : \[ f' = f \
\text{et}\ f(0) = 1.\] Cette fonction s'appelle la fonction
exponentielle et est notée $\exp$.

\subsection{Propriétés «fonctionnelles»}

Les premières propriétés relatives à la fonction exponentielle sont
celles qui explicitent la relation fonctionnelle de l'exponentielle,
c'est à dire la transformation des sommes en produits. Explicitons-les.


\begin{proposition}
  Pour tout $a$ et $b$ réels, on a \[\exp(a+ b) = \exp(a)\times\exp(b)
  .\]
  \begin{proof}
    Soit $b$ un nombre réel.

    On considère la fonction $g_b$ définie sur $\R$ par $g_b(x) =
    \frac{\exp(x+b)}{\exp(b)}$. En dérivant $g_b$ et en calculant
    $g_b(0)$, on démontre que $g_b = \exp$.
  \end{proof}
\end{proposition}

\begin{remarque}
  On appelle cette propriété un morphisme de groupe, et même plus
  précisément un morphisme de $(\R,+)$ dans le groupe $\R^*,\times)$.
\end{remarque}

En utilisant explicitement cette propriété avec des nombres négatifs, on
a le corollaire suivant.

\begin{corollaire}
  Pour tout $a$ et $b$ réels, \[ \exp(a-b) = \frac{\exp(a)}{\exp(b)} \]
\end{corollaire}

On a aussi la propriété suivante :
\begin{proposition}
  Pour tout entier naturel $n$ et pour tout nombre réel $a$, \[\exp(na)
  = (\exp(a))^n.\]
  \begin{proof}
    Par récurrence en utilisant la propriété de transformation de somme
    en produit.
  \end{proof}
\end{proposition}

Cette propriété s'étend assez naturellement aux entiers relatifs.

\begin{note}[Notation]
  Si on pose $\exp(1) = e$, on obtient pour tout entier relatif que
  $\exp(n) = e^n$. Cette notation étant très commode, on l'adopte
  souvent pour tous les nombres réels.
\end{note}

\subsection{Variations de la fonction exponentielle}

Par définition de la fonction exponentielle, $\exp' = \exp$. Les
variations de $\exp$ sont données par son signe.

\begin{proposition}
  Pour tout $x$ réel, $e^x > 0$.
  \begin{proof}
    Soit $x$ un nombre réel. $e^x$ s'écrit $e^{2\frac{x}2} =
    \left(e^{\frac{x}2}\right)$ qui est positif (c'est un carré).
  \end{proof}
\end{proposition}

On en déduit que la fonction exponentielle est strictement croissante
sur $\R$.

\begin{corollaire}
  Pour tous nombres réels $a$ et $b$, \[ a \leqslant b \iff e^a
  \leqslant e^b.\]
\end{corollaire}

Une autre conséquence, en utilisant le théorème des valeurs
intermédiaires, est que l'équation $e^x = a$ avec $a$ strictement
positif possède une unique solution sur $\R$.

\subsection{Limites de $\exp$}

\begin{proposition}
  \begin{multicols}{2}
    \begin{itemize}
      \item $\lim_{x\to+\infty}e^x = +\infty$
      \item $\lim_{x\to-\infty}e^{x} = 0$
    \end{itemize}
  \end{multicols}
\end{proposition}

Une activité a permis de démontrer la première de ces limites, la
deuxième se déduisant aisément de la précédente.

On peut également démontrer que $\exp$ «croit plus vite que n'importe
quelle puissance» et écrire les limites suivantes :

\begin{proposition}
  \begin{multicols}{3}
    \begin{itemize}
      \item $\lim_{x\to0}\frac{e^x-1}{x} = 1$
      \item $\lim_{x\to+\infty}\frac{e^{x}}x = +\infty$
      \item $\lim_{x\to-\infty}xe^{x} = 0$
    \end{itemize}
  \end{multicols}
\end{proposition}

La propriété 5, telle qu'écrite ci-dessus, est la seule démontrée en
Terminale, mais elle possède en puissance la force de sa généralisation.
Voir pour cela l'activité sur les limites de la fonction exponentielle.

\subsection{Tableau de variation et courbe représentative}

On a le tableau de variation suivant pour la fonction exponentielle :

\begin{center}
  \begin{tikzpicture}
    \tkzTabInit[espcl=9]
    {$x$ / 1 , $\exp(x)$ / 1, $\exp$ / 3}
    {$-\infty$, $+\infty$}
    \tkzTabLine{, + , }
    \tkzTabVar{-/0,+/$+\infty$}
    \tkzTabVal{1}{2}{0.33}{0}{1}
    \tkzTabVal{1}{2}{0.67}{1}{$e$}
  \end{tikzpicture}
\end{center}

Ce tableau de variation nous permet de déduire la représentation
graphique suivante :

\begin{center}
  \begin{tikzpicture}[>=latex]
    \tkzInit[xmin=-5,xmax=5,ymin=-0.5,ymax=16]
    \tkzGrid
    \tkzAxeXY

    \foreach \a in {-1,0,1,2} {
      \draw[thick,red,<->] plot[domain={\a-1}:{\a+1}] (\x, {exp(\a)*(\x-\a)
      + exp(\a)}) ;
    }

    \draw[very thick] plot[smooth,domain=-5:0] (\x,{exp(\x)}) ;
    \draw[very thick] plot[smooth,domain=0:2] (\x,{exp(\x)}) ;
    \draw[very thick] plot[smooth,domain=2:{exp(1)}] (\x,{exp(\x)}) ;

  \end{tikzpicture}
\end{center}

