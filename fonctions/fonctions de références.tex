\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[frenchb]{babel}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{pstricks}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usepackage{mathrsfs}
\usetikzlibrary{arrows}
\usetkzobj{all}
\usepackage[top=1.7cm,bottom=2cm,left=2cm,right=2cm]{geometry}



\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\makeatletter
\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \Large \@title \\
         \@author \\%
        \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
}

\newcommand{\R}{\mathbf{R}}
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\lVert #1 \rVert}
\newcommand{\vabs}[1]{\lvert #1 \rvert}

\makeatother


\theoremstyle{break}
\newtheorem{definition}{Définition}
\newtheorem{propriete}{Propriété}
\newtheorem{propdef}{Propriété - Définition}
\theoremstyle{plain}
\theorembodyfont{\normalfont}
\newtheorem{exercice}{Exercice}
\theoremstyle{nonumberplain}
\newtheorem{remarque}{Remarque}
\newtheorem{probleme}{Problème}

\newtheorem{preuve}{Preuve}



% Mise en forme des labels dans les énumérations
\usepackage{enumitem}

\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\alph{enumi}}
\renewcommand{\theenumii}{\arabic{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\title{Fonctions de référence}
\author{1S}
\date{cours}

\begin{document}


\maketitle\\

\textcolor{blue}{\section{Fonction numérique}}

\textbf{\textcolor{cyan}{Ensemble de définition}}\\
Une fonction numérique $f$ d'une variable réelle $x$ est une relation qui a un nombre réel  $x$ associe un unique réel $f(x)$. On note $f$ :$ \mapsto f(x)$.\\

L'ensemble de définition d'une fonction $f$ est l'ensemble des valeurs de la variable $x$ pour lesquelles la fonction est définie.\\

\textbf{\textcolor{cyan}{Comparaison de fonctions}}\\
On dit que deux fonctions $f$ et $g$ sont égales si et seulement si :
\begin{itemize}[label=\textbullet]
\item Elles ont le même ensemble de définition : $D_f = D_g$
\item Pour tout $x\in D_f$, $f(x) = g(x)$.\\

\end{itemize}

Soient $f$ et $g$ deux fonction définies sur un intervalle $I$.
\begin{itemize}[label=\textbullet]
\item $f$ est inférieure à $g$, si et seulement si : $\forall x\in I$, $f(x) < g(x)$.
\item $f$ est supérieure à $g$, si et seulement si : $\forall x\in I $, $f(x) > g(x)$.
\item $f$ est positive si et seulement si : $\forall x\in I$, $f(x) \geqslant 0$.
\item $f$ est négative si et seulement si : $\forall x\in I$, $f(x) \leqslant 0$.
\item $f$ admet un maximum $x_M$ si et seulement si : $\forall x\in I$, $f(x) \leqslant f(x_M)$.
\item $f$ admet un maximum $x_M$ si et seulement si : $\forall x\in I$, $f(x) \geqslant f(x_M)$.
\item $f$ admet un extremum si et seulement si $f$ admet un maximum ou un minimum.\\

\end{itemize}

\textbf{\textcolor{cyan}{Variation d'une fonction}}\\
Soit un intervalle $I$, $a$ et $b$ deux réels de $I$. Soit une fonction $f$ définie au moins sur $I$.
\begin{itemize}[label=\textbullet]
\item $f$ est croissante sur $I$ si et seulement si $a < b \Rightarrow f(a) < f(b)$.
\item $f$ est décroissante sur $I$ si et seulement si $a < b \Rightarrow f(a) > f(b)$.
\item $f$ est monotone si et seulement si $f$ est croissante ou décroissante sur $I$.\\

\end{itemize}

La courbe représentative d'une fonction $f$ dans un repère est l'ensemble des points $M(x ; f(x))$ avec $x$ un élément de l'ensemble de définition de la fonction.\\
$y = f(x)$ est une équation de la courbe représentative.\\

\textcolor{blue}{\section{Fonctions de référence}}
\textcolor{teal}{\subsection{Fonction affine}}
 Une fonction affine $f$ est une fonction définie sur $\R$ par 
\begin{center}
 $f(x) = mx + p$.\\
\end{center}
 La représentation graphique d'une fonction affine est une droite qui passe par le point de coordonnées $(0 ; p)$.\\
 $m$ est le coefficient directeur de la droite.\\
 Le point de coordonnées $(0 ; p)$ est l'ordonnée à l'origine.\\
 
  Variation et signe de la fonction : 
  \begin{multicols}{2}
  Si $m < 0$ alors la fonction $f$ est décroissante
\[\begin{array}{|c|ccccc|}
\hline
x & -\infty & & -\dfrac{p}{m} & & +\infty\\ \hline
\text{signe de } f(x) & & + & 0 & - & \\
\hline
\end{array}\]

  Si $m > 0$ alors la fonction $f$ est croissante.
\[\begin{array}{|c|ccccc|}
\hline
x & -\infty & & -\dfrac{p}{m} & & +\infty\\ \hline
\text{signe de } f(x) & & - & 0 & + & \\
\hline
\end{array}\]
\end{multicols}

 \textcolor{teal}{\subsection{Fonction du second degré}}
 On appelle fonction polynôme du second degré toute fonction définie sur $\R$ par
 \begin{center}
  $f(x) = ax^2 +bx +c$ avec $a\neq 0 $.\\
 \end{center}
 La représentation graphique d'une fonction du second degré est une parabole d'axe verticale et de sommet $S\left( -\dfrac{b}{a} ; f\left(  -\dfrac{b}{a}\right) \right) $ dirigée vers le haut si $a > 0$ et dirigée vers le bas si $a < 0$.\\
 
 Le signe d'une fonction du second degré dépend de la position de la parabole. (Discriminant, racines)\\
 \textcolor{violet}{Exercices 1, 2 , 3, 4 page 80 }

  \textcolor{teal}{\subsection{Fonction homographique}}
  On appelle fonction homographique, une fonction $f$ définie sur $\R -\lbrace\alpha\rbrace$ et qui peut se mettre sous la forme  $f(x) = \dfrac{a}{x-\alpha} + \beta$.\\
  La représentation d'une fonction homographique est une hyperbole centrée en $S(\alpha ; \beta)$.\\
  Variation : 
  \begin{multicols}{2}
  Si $a > 0$\\
  $\begin{array}{|l|*7{c}|}\hline
x&-\infty&&&\alpha&&&+\infty\\ \hline
&\beta&&&\vline\ \vline&+\infty&&\\
f(x)&&&\searrow&\vline\ \vline&&\searrow&\\
&&&-\infty&\vline\ \vline&&&\beta\\ \hline
\end{array}$

  Si $a < 0$\\
  $\begin{array}{|l|*7{c}|}\hline
x&-\infty&&&\alpha&&&+\infty\\ \hline
&&&+\infty&\vline\ \vline&&&\beta\\
f(x)&&&\nearrow&\vline\ \vline&&\nearrow&\\
&\beta&&&\vline\ \vline&-\infty&&\\ \hline
\end{array}$
\end{multicols}

 \textcolor{violet}{Exercices 5, 8, 12 page 80 }
 \textcolor{teal}{\subsection{Fonction racine carrée}}
 
 La fonction racine carrée est la fonction $f$ définie sur $\R_+$ par $f(x) = \sqrt{x}$.\\
 La fonction racine carrée est croissante sur $\R_+$.\\
 La fonction racine carrée est positive.\\

Démonstration :\\
Soit deux réels $a$ et $b$ tel que $a > b$.\\
 \'Etudier le signe de $\sqrt{a} - \sqrt{b}$ :\\
 $\sqrt{a} - \sqrt{b} = \dfrac{(\sqrt{a} - \sqrt{b})(\sqrt{a} + \sqrt{b})}{\sqrt{a} + \sqrt{b}}$\\
 $\dfrac{a - b}{\sqrt{a} +\sqrt{b}}$\\
 Par définition $\sqrt{a} +\sqrt{b}$ est positif.\\
 $a > b$ équivaut à $a - b > 0$.\\
 donc $\sqrt{a} - \sqrt{b} > 0$
 La fonction racine carrée est croissante sur $[0; +\infty[$\\
 
 \textbf{\textcolor{cyan}{Propriétés :}}\\
\begin{itemize}[label=\textbullet]
  \item Si $a \geqslant 0$ alors $\sqrt{a^2} = a$.
  \item Si $a \leqslant 0$ alors $\sqrt{a^2} = -a$.
  \item Si $a \geqslant 0$ et $b \geqslant 0$ alors $\sqrt{ab} = \sqrt{a} \times \sqrt{b}$.
    \item Si $a \geqslant 0$ et $b >0$ alors $\sqrt{\dfrac{a}{b}} = \dfrac{\sqrt{a}}{\sqrt{b}}$.
  \end{itemize}

\begin{tikzpicture}
      \tkzInit[xmin=0,xmax=8,ymax=4]
      \tkzGrid
      \tkzAxeXY
    \draw [domain=0:8][draw=red, very thick] plot  (\x, {sqrt(\x)});
 \end{tikzpicture}
 
 Exemple : Résoudre l'équation  $\sqrt{x-2}=10$\\
 Déterminer l'ensemble de définition : \\
 $x-2\geqslant 0$\\
 $D=[2; +\infty[$\\
 \'Elever les deux membres de l'égalité au carré :\\
  $x-2=100$\\
  Résoudre l'équation obtenue : $x=102$\\
  Vérification :\\
  L'ensemble de solution est $\lbrace102\rbrace$\\
 
  \textcolor{violet}{Exercices 16, 17 page 81 }
 \textcolor{teal}{\subsection{Fonction valeur absolue}}
 On appelle valeur absolue d'un nombre réel $x$, le nombre noté $\vabs{x}$ tel que : 
 \begin{center}
$\vabs{x} = x $  si $ x $ est positif.\\
$\vabs{x} = - x $ si $ x $ est négatif.\\
 \end{center}
 
  \textbf{\textcolor{cyan}{Propriétés :}}\\
\begin{itemize}[label=\textbullet]
  \item   $\forall x \in \R$ on a $\vabs{x} \geqslant 0$.
  \item $\forall x \in \R$ on a $\vabs{x} = 0$ équivaut à $x = 0$
  \item $\forall x \in \R$ on a $\vabs{-x} = \vabs{x}$.
  \item $\forall x \in \R$ on a $\vabs{x^2} = x^2$.
  \item $\forall x \in \R$ on a $\vabs{x}^2 = x^2$
  \item $\forall x\in \R$ et $\forall y\in \R$ on a $\vabs{xy} = \vabs{x} \times \vabs{y}$.
  \item $\forall x\in \R$ et $\forall y\in \R$ on a $\vabs{x + y} \leqslant \vabs{x} + \vabs{y}$. Inégalité triangulaire
  \item $\forall x\in \R$et $\forall y\in \R$ on a $\vabs{x} = \vabs{y}$ équivaut à $x = y$ ou $x = -y$.
  \item $\forall x \in \R$ on a $\sqrt{x^2} = \vabs{x}$.
      
  \end{itemize}

Exemple : \\
Résoudre dans $\R$ l'équation suivante : $\vabs{2x - 2} = \vabs{3 - x}$\\
$\forall x\in\R$ et $\forall y\in \R$ on a $\vabs{x} = \vabs{y}$ équivaut à $x = y$ ou $x = -y$.\\
On a alors : $2x - 2 = 3 - x$ ou $2x - 2 = -3 + x$\\
$\Leftrightarrow x = \dfrac{5}{3}$ ou $x = -1$.\\

 \textbf{\textcolor{cyan}{Variation :}}\\

La fonction valeur absolue est une fonction affine définie par morceaux. Sa représentation graphique est composées de deux demi-droite.\\
La fonction valeur absolue est strictement décroissante sur l'intervalle ] $-\infty$ ; 0].\\
La fonction valeur absolue est strictement croissante sur l'intervalle [0 ; $+\infty$ [.\\
La courbe représentative de la fonction valeur absolue a pour axe de symétrie l'axe des ordonnées.\\
\begin{tikzpicture}
      \tkzInit[xmin=-4,xmax=4,ymin=-2,ymax=4]
      \tkzGrid
      \tkzAxeXY
    \draw [domain=0:4] [draw=red, very thick] plot  (\x, \x);
     \draw [domain=-4:0] [draw=red, very thick] plot  (\x, {-\x});
 \end{tikzpicture}
 
 \[\begin{array}{|c|c c c c c|}
\hline
x& -\infty & & 0 & & +\infty \\ \hline
&+\infty & &  & &+\infty \\
f(x) & & \searrow & & \nearrow & \\
&  & & 0& &  \\
\hline
\end{array}\]\\

La fonction valeur absolue $\vabs{x - a}$ représente la distance de $x$ au nombre $a$.\\
Soient $a$ un nombre réel et $r$ un nombre réel strictement positif. On a : $\vabs{x - a} \leqslant r$ si et seulement si $x \in [a - r ; a + r]$.\\

Exemple : Résoudre $\vabs{x - 2} < 1$\\
L'inéquation $\vabs{x - 2} < 1$ équivaut à la distance entre le nombre $x$ et 2 qui doit être strictement supérieur à 1.\\
 $\vabs{x - a} \leqslant r$ si et seulement si $x \in [a - r ; a + r]$.\\
  $\vabs{x - 2} \leqslant 1$ si et seulement si $x \in [2 -1 ; 2 + 1]$.\\
$S = ]-\infty ; 1[ \cup ]3 ; + \infty[$.\\

 \textcolor{violet}{Exercices 20, 22, 23, 24, 25, 27 page 81 - 82 }

\textcolor{blue}{\section{Fonctions associées}}
 \textcolor{teal}{\subsection{Fonction $u + k$}}
 Soit une fonction $u$ définie sur un ensemble $D$ et $k$ un nombre réel.\\
 La fonction $u + k$ est définie sur l'ensemble $D$ par $(u + k)(x) = u(x) + k$.
 Les fonctions $u$ et $u + k$ ont le même sens de variation.\\
 La représentation graphique de la fonction $u + k$ se déduit de la représentation graphique de la fonction $u$ en faisant une translation de vecteur $\Vecteur{v}\left(\begin{array}{c}0\\ k\end{array}\right) $.\\
 
  \textcolor{teal}{\subsection{Fonction $\lambda u$}}
  Soit une fonction $u$ définie sur un ensemble $D$ et $\lambda$ un nombre réel.\\
  La fonction $\lambda u$ est définie sur l'ensemble $D$ par $(\lambda u)(x) = \lambda \times u(x)$.\\
\begin{itemize}[label=\textbullet]
  \item Si $\lambda > 0$, les fonctions $u$ et $\lambda u$ ont le même sens de variation.
  \item Si $\lambda < 0$, les fonctions $u$ et $\lambda u$ ont des sens de variation contraire.
  \item Si $\lambda = 0$, la fonction $\lambda u$ est la fonction constante nulle.\\
  
  \end{itemize}
  
   \textcolor{violet}{Exercices 43, 45, 46  page 83 }
\textcolor{teal}{\subsection{Fonction $\sqrt(u)$}}
    Soit une fonction $u$ définie sur un ensemble $D$ telle que pour tout $x\in D$, $u(x) \geqslant 0$.\\
     La fonction $\sqrt(u)$ est définie sur l'ensemble $D$ par $\sqrt{u(x)}$.\\
    Les fonctions $u$ et $\sqrt{u}$ ont le même sens de variation.\\
     
Démonstration : \\
  Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est croissante donc $u(a) > u(b)$.\\
  La fonction racine carrée est croissante sur l'intervalle $[0; +\infty[ $ donc $\sqrt{u(a)} > \sqrt{u(b)}$.
  On a donc que la fonction $\sqrt{u}$ est croissante.\\
      Les fonctions $u$ et $\sqrt{u}$ ont le même sens de variation.\\
  Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est décroissante donc $u(a) < u(b)$.\\
  La fonction racine carrée est croissante sur l'intervalle  $[0; +\infty[ $ donc $\sqrt{u(a)} < \sqrt{u(b)}$.
  On a donc que la fonction $\sqrt{u}$ est décroissante.\\
      Les fonctions $u$ et $\sqrt{u}$ ont le même sens de variation.\\
 
  
\textcolor{teal}{\subsection{Fonction $\dfrac{1}{u}$}}
    Soit une fonction $u$ définie sur un ensemble $D$ telle que pour tout $x\in D$, $u(x) \neq 0$.\\
    La fonction $\dfrac{1}{u}$ est définie sur l'ensemble $D$ par $\dfrac{1}{u}(x) = \dfrac{1}{u(x)}$.\\
\begin{itemize}[label=\textbullet]
   \item Soit $u$ une fonction strictement positive, les fonctions $u$ et $\dfrac{1}{u}$ ont des sens de variation contraires.
   \item Soit $u$ une fonction strictement négative, les fonctions $u$ et $\dfrac{1}{u}$ ont des sens de variation contraires.\\
   
\end{itemize}    
Démonstration : \\
  Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est croissante et strictement positive donc $u(a) > u(b)$.\\
  La fonction inverse est décroissante  sur l'intervalle $]0; +\infty[$,  alors $\dfrac{1}{u(a)} < \dfrac{1}{u(b)}$.\\
  La fonction $\dfrac{1}{u}$ est décroissante.\\
  Soit $u$ une fonction strictement positive, les fonctions $u$ et $\dfrac{1}{u}$ ont des sens de variation contraires.


  Soit deux réels $a$ et $b$ tel que $a > b$.\\
  Si la fonction $u$ est croissante et strictement négative donc $u(a) > u(b)$.\\
  La fonction inverse est décroissante  sur l'intervalle $]-\infty ; 0[$,  alors $\dfrac{1}{u(a)} < \dfrac{1}{u(b)}$.\\
  La fonction $\dfrac{1}{u}$ est décroissante.\\
  Soit $u$ une fonction strictement positive, les fonctions $u$ et $\dfrac{1}{u}$ ont des sens de variation contraires.\\
  
   \textcolor{violet}{Exercices 52, 53 page 84 }
\end{document}



